﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    [Range(0,7)]
    public int lod = 6;
    [Range(2,256)]
    private int resolution = 128;
    public bool autoUpdate = true;
    public enum FaceRenderMask {Top, Bottom, Left, Right, Front, Back, All};
    public FaceRenderMask faceRenderMask = FaceRenderMask.All;

    public ShapeSettings shapeSettings;
    public ColourSettings colourSettings;

    public float rotationSpeed = 4;

    [HideInInspector]
    public bool shapeSettingsFoldout;
    [HideInInspector]
    public bool colourSettingsFoldout;

    ShapeGenerator shapeGenerator = new ShapeGenerator();
    ColourGenerator colourGenerator = new ColourGenerator();

    [SerializeField, HideInInspector]
    MeshFilter[] meshFilters;
    TerrainFace[] terrainFaces;

    void Initialize()
    {
        switch (lod)
        { 
            case 0: resolution = 2; break;
            case 1: resolution = 4; break;
            case 2: resolution = 8; break;
            case 3: resolution = 16; break;
            case 4: resolution = 32; break;
            case 5: resolution = 64; break;
            case 6: resolution = 128; break;
            case 7: resolution = 256; break;
            default: resolution = 128; break;
        }

        shapeGenerator.UpdateSettings(shapeSettings);
        colourGenerator.UpdateSettings(colourSettings);
        if(meshFilters == null || meshFilters.Length == 0)
        {
            meshFilters = new MeshFilter[6];
        }
        
        terrainFaces = new TerrainFace[6];

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

        for (int i = 0; i < 6; i++)
        {
            if (meshFilters[i] == null)
            {
                GameObject meshObj = new GameObject("mesh");
                meshObj.transform.parent = transform;

                meshObj.AddComponent<MeshRenderer>();
                meshFilters[i] = meshObj.AddComponent<MeshFilter>();
                meshFilters[i].sharedMesh = new Mesh();
            }
            meshFilters[i].GetComponent<MeshRenderer>().sharedMaterial = colourSettings.planetMaterial;

            terrainFaces[i] = new TerrainFace(shapeGenerator, meshFilters[i].sharedMesh, resolution, directions[i]);
            bool renderFace = faceRenderMask == FaceRenderMask.All || (int) faceRenderMask == i;
            meshFilters[i].gameObject.SetActive(renderFace);
        }
    }

    public void GeneratePlanet()
    {
        if (shapeSettings == null)
        {
            shapeSettings = ScriptableObject.CreateInstance("ShapeSettings") as ShapeSettings;
            shapeSettings.Initialize();
            shapeSettings.RandomizeShapeSettings(Enums.PlanetType.Random);
        }
        if (colourSettings == null)
        {
            colourSettings = ScriptableObject.CreateInstance("ColourSettings") as ColourSettings;
            colourSettings.Initialize();
            colourSettings.RandomizeLandColor(Enums.SmoothnessType.Smooth);
            colourSettings.RandomizeOceanColor(Enums.SmoothnessType.Smooth);
        }   
        OnShapeSettingsUpdated();
        OnColourSettingsUpdated();
    }

    public void OnShapeSettingsUpdated()
    {
        if(autoUpdate)
        {
            Initialize();
            GenerateMesh();
        }  
    }

    public void OnColourSettingsUpdated()
    {
        if (autoUpdate)
        {
            colourSettings.planetMaterial.SetFloat("_smth", colourSettings.smoothness);
            Initialize();
            GenerateColours();
        }
    }

    void GenerateMesh()
    {
        for(int i = 0; i < 6; i++)
        {
            if (meshFilters[i].gameObject.activeSelf)
            {
                terrainFaces[i].ConstructMesh();
            }
        }

        colourGenerator.UpdateElevation(shapeGenerator.elevationMinMax);
    }

    void GenerateColours()
    {
        colourGenerator.UpdateColours();
        for (int i = 0; i < 6; i++)
        {
            if (meshFilters[i].gameObject.activeSelf)
            {
                terrainFaces[i].UpdateUVs(colourGenerator);
            }
        }
    }

    public void RandomizePlanetsShape(Enums.PlanetType pType)
    {
        shapeSettings.RandomizeShapeSettings(pType);
        OnShapeSettingsUpdated();
    }

    public void RandomizeBiomesMaterial(Enums.SmoothnessType sType)
    {
        colourSettings.CreateRandomBiomes(sType);
        OnShapeSettingsUpdated();
        OnColourSettingsUpdated();
    }

    public void RandomizeOceanMaterial(Enums.SmoothnessType sType)
    {
        colourSettings.RandomizeOceanColor(sType);
        OnColourSettingsUpdated();
    }

    public void RandomizePlanetsMaterial(Enums.SmoothnessType sType)
    {
        colourSettings.CreateRandomBiomes(sType);
        colourSettings.RandomizeOceanColor(sType);
        OnColourSettingsUpdated();
    }

    private void Start()
    {
        OnColourSettingsUpdated();
    }

    private void Update()
    {        
        //transform.Rotate(Vector3.up * (rotationSpeed * Time.deltaTime));
    }
}
