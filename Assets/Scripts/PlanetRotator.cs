﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlanetRotator : MonoBehaviour, IDragHandler
{
    [SerializeField] private Transform planet;

    public void OnDrag(PointerEventData eventData)
    {
        planet.localEulerAngles += new Vector3(-eventData.delta.y * 0.25f, -eventData.delta.x * 0.25f);

    }
}
