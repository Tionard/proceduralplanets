﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[CreateAssetMenu(menuName = "Planet/PlanetArchive")]
[System.Serializable]
public class PlanetsArchive : ScriptableObject
{
    [SerializeField]
    public List<PlanetPreset> planetPresets;
    [SerializeReference]
    public List<ShapeSettings> sS;

    internal void Initialize()
    {
        if(planetPresets == null)
        {
            planetPresets = new List<PlanetPreset>();
        }
    }

    [System.Serializable]
    public class PlanetPreset
    {
        [SerializeField]
        public ShapeSettings shapeSettings;
        [SerializeField]
        public ColourSettings colorSettings;
        public string presetName;
    }

    public void SaveNewPreset(string name, ShapeSettings shape, ColourSettings color)
    {
        PlanetPreset presetBuf = GetPreset(name);
        if (presetBuf == null)
        {
            presetBuf = new PlanetPreset();
            planetPresets.Add(presetBuf);
        }
        presetBuf.presetName = name;
        presetBuf.shapeSettings = Instantiate(shape) as ShapeSettings;
        sS.Add(shape);
        presetBuf.colorSettings = Instantiate(color) as ColourSettings;
        UpdatePreset(presetBuf);
    }

    public void UpdatePreset(PlanetPreset preset)
    {
        foreach (PlanetPreset p in planetPresets)
        {
            if(p.presetName == preset.presetName)
            {
                p.shapeSettings = preset.shapeSettings;
                p.colorSettings = preset.colorSettings;
            }
        }
    }

    public void UpdatePresetColor(PlanetPreset preset)
    {
        foreach (PlanetPreset p in planetPresets)
        {
            if (p.presetName == preset.presetName)
            {
                p.colorSettings = preset.colorSettings;
            }
        }
    }

    public void UpdatePresetShape(PlanetPreset preset)
    {
        foreach (PlanetPreset p in planetPresets)
        {
            if (p.presetName == preset.presetName)
            {
                p.shapeSettings = preset.shapeSettings;
            }
        }
    }

    public PlanetPreset GetPreset(string name)
    {
        foreach(PlanetPreset p in planetPresets)
        {
            if(p.presetName == name)
            {
                return p;
            }
        }
        return null;
    }

    public void SaveToJson()
    {
        string json = JsonUtility.ToJson(planetPresets);
        Debug.Log(json);
        File.WriteAllText(Application.dataPath + "/saveFile.json", json);
    }
    public void LoadFromJson()
    {
        string json = File.ReadAllText(Application.dataPath + "/saveFile.json");
        List<PlanetPreset> List = JsonUtility.FromJson<List<PlanetPreset>>(json);
        Debug.Log(List);
    }
}
