﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField] private Transform target;


    private void Start()
    {
        this.GetComponent<Camera>().rect = new Rect(0.31f, 0f, 0.69f, 1f);
    }
    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKey("space"))
        {
            transform.LookAt(target);
            transform.Translate(Vector3.right * Time.deltaTime);
        }
#endif
    }

}
