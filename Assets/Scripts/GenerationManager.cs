﻿using NaughtyAttributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GenerationManager : MonoBehaviour
{
    public Planet planet;
    [HorizontalLine(2, EColor.Red)]
    public TMP_Text presetNameText; 
    public TMP_Dropdown presetDropdown;
    [HorizontalLine(2, EColor.Red)]
    public TMP_Dropdown planetTypeDropdown;
    [HorizontalLine(2, EColor.Red)]
    public TMP_Dropdown smoothnessTypeDropdown;
    [HorizontalLine(2, EColor.Red)]
    public TMP_Dropdown noiseLayerDropdown;
    public Slider strengthSlider;
    public Slider baseNoiseSlider;
    public Slider rougnessSlider;
    public Slider persistanceSlider;
    public Slider minValueSlider;
    [HorizontalLine(2, EColor.Red)]
    public TMP_Dropdown biomeSelectionDropdown;
    public TMP_Dropdown landNormalMapDropdown;
    public Slider landNormalMapTiling;
    public TMP_Dropdown waterNormalMapDropdown;
    public Slider waterNormalMapTiling;
    public Slider reflectivenessSlider;
    [HorizontalLine(2, EColor.Red)]
    public Camera cam;
    public Slider lodSlider;
    public Slider camDistanceSlider;
    public Slider rotationSpeedSlider;
    public Slider musicVolumeSlider;
    [HorizontalLine(2, EColor.Red)]

    private string selectedPreset;
    private Enums.PlanetType planetType = Enums.PlanetType.Continential;
    private Enums.SmoothnessType smoothnessType = Enums.SmoothnessType.Smooth;
    private WordGenerator wordGen;
    [HorizontalLine(2, EColor.Red)]
    public Button loadMaterialButton;
    public Button loadMeshButton;
    public AudioSource audioPlayer;

    private bool fullPresetLoading = false;
    private void Start()
    {
        ResourcesManager.Initialize();
        updatePresetList();
        SetSelectedPreset();
        PopulateElements();
        RandomizeAll();
        wordGen = new WordGenerator();
        
    }

    private void RepopulateBiomSelectionDropdown()
    {
        MuteCalls(biomeSelectionDropdown.onValueChanged);
        biomeSelectionDropdown.ClearOptions();
        List<string> biomesAmount = new List<string>();
        for (int i = 0; i < planet.colourSettings.biomeColourSettings.biomes.Length; i++)
        {
            biomesAmount.Add("" + i);
        }
        biomeSelectionDropdown.AddOptions(biomesAmount);
        UnmuteCalls(biomeSelectionDropdown.onValueChanged);
    }

    public void ChangeBiomeGradientButtonClick()
    {
        GradientPicker.Create(planet.colourSettings.biomeColourSettings.biomes[biomeSelectionDropdown.value].gradient, 
            "Biome" + biomeSelectionDropdown.value + " Gradient", SetBiomeGradient, BiomeGradientFinished);
    }

    private void SetBiomeGradient(Gradient currentGradient)
    {
        planet.colourSettings.biomeColourSettings.biomes[biomeSelectionDropdown.value].gradient = currentGradient;
        planet.OnColourSettingsUpdated();
    }

    private void BiomeGradientFinished(Gradient finishedGradient)
    {
        planet.colourSettings.biomeColourSettings.biomes[biomeSelectionDropdown.value].gradient = finishedGradient;
        planet.OnColourSettingsUpdated();
    }

    public void ChangeOceanGradientButtonClick()
    {
        GradientPicker.Create(planet.colourSettings.oceanColour, "Ocean's Gradient", SetOceanGradient, OceanGradientFinished);
    }
    private void SetOceanGradient(Gradient currentGradient)
    {
        planet.colourSettings.oceanColour = currentGradient;
        planet.OnColourSettingsUpdated();
    }

    private void OceanGradientFinished(Gradient finishedGradient)
    {
        planet.colourSettings.oceanColour = finishedGradient;
        planet.OnColourSettingsUpdated();
    }

    public void RandomizeAll()
    {
        RandomizePlanetsShape();
        RandomizePlanetsMaterial();
        RandomizeShapeOrigin();
    }

    public void RandomizePlanetsShape()
    {
        planet.RandomizePlanetsShape(planetType);
        RepopulateNoiseLayersDropdown();
        UpdateSettingsSliderElements();
    }
    public void RandomizeBiomesMaterial()
    {
        planet.RandomizeBiomesMaterial(smoothnessType);
        UpdateLandNormalElements();
        RepopulateBiomSelectionDropdown();

    }
    public void RandomizeOceanMaterial()
    {
        planet.RandomizeOceanMaterial(smoothnessType);
        UpdateReflectivenessSlider();
        UpdateWaterNormalElementes();
    }
    public void RandomizePlanetsMaterial()
    {
        RandomizeBiomesMaterial();
        RandomizeOceanMaterial();
    }

    public void RandomizeShapeOrigin()
    {
        planet.shapeSettings.RandomizeNoiseOrigin();
        planet.OnShapeSettingsUpdated();
    }


    public void SetNewLOD(float value)
    {
        planet.lod = (int) value;
        planet.OnShapeSettingsUpdated();
        planet.OnColourSettingsUpdated();
        //ResourcesManager.ExportTexture(planet.colourSettings.planetMaterial);
    }

    public void SetNewRotationSpeed(float value)
    {
        planet.rotationSpeed = value;
    }

    public void SetNewMusicVolume(float value)
    {
        audioPlayer.volume = value;
    }

    public void SetNewCameraDistance(float value)
    {
        var camPos = cam.transform.position;
        cam.GetComponent<Transform>().position = new Vector3(camPos.x, camPos.y, value);
    }

    private void RepopulateNoiseLayersDropdown()
    {
        MuteCalls(noiseLayerDropdown.onValueChanged);
        noiseLayerDropdown.ClearOptions();
        List<string> layersAmount = new List<string>();
        for (int i = 0; i < planet.shapeSettings.noiseLayers.Length; i++)
        {
            layersAmount.Add("" + i);            
        }
        noiseLayerDropdown.AddOptions(layersAmount);
        noiseLayerDropdown.value = 0;
        UnmuteCalls(noiseLayerDropdown.onValueChanged);
    }

    private void UpdateSettingsSliderElements()
    {

        MuteCalls(baseNoiseSlider.onValueChanged);
        MuteCalls(rougnessSlider.onValueChanged);
        MuteCalls(persistanceSlider.onValueChanged);
        MuteCalls(minValueSlider.onValueChanged);
        MuteCalls(strengthSlider.onValueChanged);
        NoiseSettings ns = planet.shapeSettings.noiseLayers[noiseLayerDropdown.value].noiseSettings;
        if (ns.filterType == Enums.FilterType.Simple)
        {
            strengthSlider.value = ns.simpleNoiseSettings.strenght;
            baseNoiseSlider.value = ns.simpleNoiseSettings.baseRoughness;
            rougnessSlider.value = ns.simpleNoiseSettings.roughness;
            persistanceSlider.value = ns.simpleNoiseSettings.persistance;
            minValueSlider.value = ns.simpleNoiseSettings.minValue;
        }
        else
        {
            strengthSlider.value = ns.rigidNoiseSettings.strenght;
            baseNoiseSlider.value = ns.rigidNoiseSettings.baseRoughness;
            rougnessSlider.value = ns.rigidNoiseSettings.roughness;
            persistanceSlider.value = ns.rigidNoiseSettings.persistance;
            minValueSlider.value = ns.rigidNoiseSettings.minValue;
        }
        UnmuteCalls(baseNoiseSlider.onValueChanged);
        UnmuteCalls(rougnessSlider.onValueChanged);
        UnmuteCalls(persistanceSlider.onValueChanged);
        UnmuteCalls(minValueSlider.onValueChanged);
        UnmuteCalls(strengthSlider.onValueChanged);
    }

    private void UpdateLandNormalElements()
    {
        MuteCalls(landNormalMapTiling.onValueChanged);
        MuteCalls(landNormalMapDropdown.onValueChanged);
        landNormalMapDropdown.value = landNormalMapDropdown.options.FindIndex(option => option.text == planet.colourSettings.landNormalMap.name);
        landNormalMapTiling.value = planet.colourSettings.landNMTiling;
        UnmuteCalls(landNormalMapTiling.onValueChanged);
        UnmuteCalls(landNormalMapDropdown.onValueChanged);
    }

    private void UpdateWaterNormalElementes()
    {
        MuteCalls(waterNormalMapTiling.onValueChanged);
        MuteCalls(waterNormalMapDropdown.onValueChanged);
        waterNormalMapDropdown.value = waterNormalMapDropdown.options.FindIndex(option => option.text == planet.colourSettings.waterNormalMap.name);
        waterNormalMapTiling.value = planet.colourSettings.waterNMTiling;
        UnmuteCalls(waterNormalMapTiling.onValueChanged);
        UnmuteCalls(waterNormalMapDropdown.onValueChanged);
    }

    private void UpdateReflectivenessSlider()
    {
        MuteCalls(reflectivenessSlider.onValueChanged);
        reflectivenessSlider.value = planet.colourSettings.smoothness;
        UnmuteCalls(reflectivenessSlider.onValueChanged);
    }
    private void updatePresetList()
    {
        presetDropdown.ClearOptions();
        presetDropdown.AddOptions(ResourcesManager.presetNames);
    }
    private void PopulateElements()
    {
        string[] pTypesArray = Enum.GetNames(typeof(Enums.PlanetType));
        List<string> planetTypes = new List<string>(pTypesArray);
        planetTypeDropdown.AddOptions(planetTypes);

        string[] smTypesArray = Enum.GetNames(typeof(Enums.SmoothnessType));
        List<string> smTypes = new List<string>(smTypesArray);
        smoothnessTypeDropdown.AddOptions(smTypes);

        //Shape Settings
        RepopulateNoiseLayersDropdown();
        UpdateSettingsSliderElements();

        //MaterialSettings
        ResourcesManager.UpdateTextures();
        landNormalMapDropdown.AddOptions(ResourcesManager.textureLandNames);
        waterNormalMapDropdown.AddOptions(ResourcesManager.textureWaterNames);
        UpdateLandNormalElements();
        UpdateWaterNormalElementes();
        UpdateReflectivenessSlider();
        RepopulateBiomSelectionDropdown();

        //GeneralSettings
        lodSlider.value = planet.lod;
        camDistanceSlider.value = Camera.main.GetComponent<Transform>().position.z;
        rotationSpeedSlider.value = planet.rotationSpeed;
        musicVolumeSlider.value = audioPlayer.volume;
    }
    public void SetNewNoiseSettings()
    {
        NoiseSettings ns = planet.shapeSettings.noiseLayers[noiseLayerDropdown.value].noiseSettings;
        if (ns.filterType == Enums.FilterType.Simple)
        {
            ns.simpleNoiseSettings.strenght = strengthSlider.value;
            ns.simpleNoiseSettings.baseRoughness = baseNoiseSlider.value;
            ns.simpleNoiseSettings.roughness = rougnessSlider.value;
            ns.simpleNoiseSettings.persistance = persistanceSlider.value;
            ns.simpleNoiseSettings.minValue = minValueSlider.value;
        }
        else
        {
            ns.rigidNoiseSettings.strenght = strengthSlider.value;
            ns.rigidNoiseSettings.baseRoughness = baseNoiseSlider.value;
            ns.rigidNoiseSettings.roughness = rougnessSlider.value;
            ns.rigidNoiseSettings.persistance = persistanceSlider.value;
            ns.rigidNoiseSettings.minValue = minValueSlider.value;
        }
        planet.OnShapeSettingsUpdated();
    }

    public void SetNewReflectiveness(float value)
    {
        planet.colourSettings.SetReflectiveness(value);
        planet.OnColourSettingsUpdated();
    }


    public void SetNewLandNormalMap()
    {
        var name = landNormalMapDropdown.captionText.text;
        Texture2D texture = ResourcesManager.texturesLandList.Find(txL => txL.name == name);
        planet.colourSettings.SetNewLandNormal(texture, landNormalMapTiling.value);
        planet.OnColourSettingsUpdated();
    }

    public void SetNewWaterNormalMap()
    {
        var name = waterNormalMapDropdown.captionText.text;
        Texture2D texture = ResourcesManager.texturesWaterList.Find(txW => txW.name == name);
        planet.colourSettings.SetNewWaterNormal(texture, waterNormalMapTiling.value);
        planet.OnColourSettingsUpdated();
    }

    public void SetPlanetType(int index)
    {
        planetType = (Enums.PlanetType) index;          
    }

    public void SetGradientSmoothnessType(int index)
    {
        smoothnessType = (Enums.SmoothnessType) index;
    }

    public void SaveShape()
    {
        string presetName = presetNameText.text;
        ResourcesManager.SaveCustomShapePreset(presetName, planet.shapeSettings);
        updatePresetList();
    }

    public void SaveColor()
    {
        string presetName = presetNameText.text;
        ResourcesManager.SaveCustomColorPreset(presetName, planet.colourSettings);
        updatePresetList();
    }

    public void SavePreset()
    {
        SaveShape();
        SaveColor();
    }

    public void LoadShape()
    {
        planet.shapeSettings = Instantiate(ResourcesManager.GetShapeSetting(selectedPreset));
        planet.OnShapeSettingsUpdated();
        RepopulateNoiseLayersDropdown();
        UpdateSettingsSliderElements();
    }

    public void LoadColor()
    {
        planet.colourSettings = Instantiate(ResourcesManager.GetColorSetting(selectedPreset));
        planet.OnColourSettingsUpdated();

        if (!fullPresetLoading)
            planet.OnShapeSettingsUpdated();

        UpdateWaterNormalElementes();
        UpdateLandNormalElements();
        UpdateReflectivenessSlider();
        RepopulateBiomSelectionDropdown();
    }

    public void LoadPreset()
    {
        fullPresetLoading = true;
        LoadColor();
        LoadShape();
        fullPresetLoading = false;
    }

    public void SetSelectedPreset()
    {
        selectedPreset = presetDropdown.captionText.text;

        loadMaterialButton.interactable = ResourcesManager.colorPresets.ContainsKey(selectedPreset);
        loadMeshButton.interactable = ResourcesManager.shapePresets.ContainsKey(selectedPreset);
    }

    private void MuteCalls(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
        }
    }

    private void UnmuteCalls(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
        }
    }

    public void GenerateRandomPresetName()
    {
        bool twoWords = UnityEngine.Random.Range(0, 4) == 1 ? true : false;
        string newName = "Planet ";
        if (twoWords)
        {
            newName += wordGen.CreateRandomWord(UnityEngine.Random.Range(3, 4), 2, 1);
            newName += " " + wordGen.CreateRandomWord(UnityEngine.Random.Range(3, 6), 2, 2);
        }
        else
        {
            newName += wordGen.CreateRandomWord(UnityEngine.Random.Range(3, 8), 2, 2);
        }
        
        presetNameText.transform.parent.parent.GetComponent<TMP_InputField>().text = newName;
    }
}
