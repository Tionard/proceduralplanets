﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    public GameObject SavePresetPopup;
    public GameObject LoadPresetPopup;
    public GameObject shapeSettingsPanel;
    public GameObject materiallSettingsPanel;
    public GameObject randomizeShapeSettingsPanel;
    public GameObject randomizeMateriallSettingsPanel;
    public GameObject generalSettingsPanel;
    public GameObject saveLoadPanel;
    public GameObject uiBlocker;
    public GameObject uiBlockerSmall;
    public GameObject GradientPicker;
    public GameObject ColorPicker;

    private bool loadPresetPopupActive;
    private bool savePresetPopupActive;

    public void SwitchSavePresetPopupActive()
    {
        savePresetPopupActive = !savePresetPopupActive;
        SavePresetPopup.SetActive(!SavePresetPopup.activeSelf);
    }

    public void SwitchLoadPresetPopupActive()
    {
        loadPresetPopupActive = !loadPresetPopupActive;
        LoadPresetPopup.SetActive(!LoadPresetPopup.activeSelf);
    }

    public void SwitchGeneralSettingsActive()
    {
        generalSettingsPanel.SetActive(!generalSettingsPanel.activeSelf);
    }

    public void SwitchShapeSettingsActive()
    {
        shapeSettingsPanel.SetActive(!shapeSettingsPanel.activeSelf);
    }

    public void SwitchMaterialSettingsActive()
    {
        materiallSettingsPanel.SetActive(!materiallSettingsPanel.activeSelf);
    }

    public void SwitchRandomShapeSettingsActive()
    {
        randomizeShapeSettingsPanel.SetActive(!randomizeShapeSettingsPanel.activeSelf);
    }

    public void SwitchRandomMaterialSettingsActive()
    {
        randomizeMateriallSettingsPanel.SetActive(!randomizeMateriallSettingsPanel.activeSelf);
    }

    public void SwitchSaveLoadPanelActive()
    {
        saveLoadPanel.SetActive(!saveLoadPanel.activeSelf);
    }

    private void Update()
    {
        updateUIBlocker();
    }
    public void updateUIBlocker()
    {
        uiBlocker.SetActive(SavePresetPopup.activeSelf | LoadPresetPopup.activeSelf | GradientPicker.activeSelf | ColorPicker.activeSelf);
        uiBlockerSmall.SetActive(SavePresetPopup.activeSelf | LoadPresetPopup.activeSelf | GradientPicker.activeSelf | ColorPicker.activeSelf);
    }


}
