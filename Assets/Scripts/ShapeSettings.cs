﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Planet/PlanetShape")]
[System.Serializable]
public class ShapeSettings : ScriptableObject
{
    [Range(1, 10)]
    public float planetRadius;

    [HorizontalLine(color: EColor.Blue)]
    public NoiseLayer[] noiseLayers;

    [SerializeField]
    [HorizontalLine(color: EColor.Blue)]
    private bool generationMode;

    [System.Serializable]
    public class NoiseLayer
    {
        public bool enabled = true;
        public bool useFirstLayerAsMask = false;
        public NoiseSettings noiseSettings = new NoiseSettings();
    }

    public void Initialize()
    {
        noiseLayers = new NoiseLayer[1];
        noiseLayers[0] = new NoiseLayer();
        //noiseLayers[0].noiseSettings.simpleNoiseSettings = new NoiseSettings.SimpleNoiseSettings();
    }

    [ShowIf("generationMode")]
    [Button("Randomize Noise Origin")]
    public void RandomizeNoiseOrigin()
    {
        foreach (NoiseLayer nl in noiseLayers)
        {
            if (nl.noiseSettings.filterType == Enums.FilterType.Simple)
            {
                nl.noiseSettings.simpleNoiseSettings.RandomizeNoiseOrigin();
            }
            else
            {
                nl.noiseSettings.rigidNoiseSettings.RandomizeNoiseOrigin();
            }
        }
    }

    //[Button("Randomize Shape Settings")]
    public void RandomizeShapeSettings(Enums.PlanetType pType)
    {
        foreach(NoiseLayer nl in noiseLayers)
        {
            
            if(nl.noiseSettings.filterType == Enums.FilterType.Simple)
            {
                nl.noiseSettings.simpleNoiseSettings.RandomizeAllValues(pType);
            }
            else
            {
                nl.noiseSettings.rigidNoiseSettings.RandomizeAllValues(pType);
            }
        }
    }

    [ShowIf("generationMode")]
    [Button("Generate Random Shape")]
    public void GenerateRandomShape()
    {
        int numberOfLayers = Random.Range(2, 3);
        noiseLayers = new NoiseLayer[numberOfLayers];
        
        for(int i = 0; i< numberOfLayers; i++)
        {
            noiseLayers[i] = new NoiseLayer();       
            if ( i == 0)
            {
                noiseLayers[i].useFirstLayerAsMask = false;
                noiseLayers[i].noiseSettings.filterType = Enums.FilterType.Simple;
                noiseLayers[i].noiseSettings = new NoiseSettings(Enums.FilterType.Simple);
            } 
            else
            {
                noiseLayers[i].useFirstLayerAsMask = true;
                noiseLayers[i].noiseSettings.filterType = Enums.FilterType.Rigid;
                noiseLayers[i].noiseSettings = new NoiseSettings(Enums.FilterType.Rigid);
            }
            
        }
        RandomizeShapeSettings(Enums.PlanetType.Random);
    }

    private void OnEnable()
    {
        planetRadius = 1f;
    }
}
