﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RemoveText : MonoBehaviour
{
    public TMP_InputField text;

    public void RemoveTextConditionaly()
    {
        text.text = "";
    }

}
