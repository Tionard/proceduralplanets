﻿using UnityEngine;

[System.Serializable]
public class NoiseSettings 
{
    public Enums.FilterType filterType;

    [ConditionalHide("filterType", 0)]
    public SimpleNoiseSettings simpleNoiseSettings;
    [ConditionalHide("filterType", 1)]
    public RigidNoiseSettings rigidNoiseSettings;

    [System.Serializable]
    public class SimpleNoiseSettings
    {
        [Range(0, 0.75f)]
        public float strenght = 0.2f;  //noise Strenght
        [Range(1, 8)]
        public int numLayers = 8;   //more layers - more details
        [Range(0, 4)]
        public float baseRoughness = 1; //1st noise stage (big chunks)
        [Range(0, 4)]
        public float roughness = 2;     //2nd noise stage (roughness of big chanks)
        [Range(0, 1f)]
        public float persistance = 0.3f;    //it can also act like smoothening
        public Vector3 center = new Vector3(0,0,0);      //noise origin
        [Range(0, 4)]
        public float minValue = 0.65f;      //cap for "flat" surface

        #region Private MinMax values

        public Enums.PlanetType planetType;

        protected float minStrenght = 0f;
        protected float maxStrenght = 0.5f;

        protected float minBaseRough = 0f;
        protected float maxBaseRough = 3f;

        protected float minRoughness = 0f;
        protected float maxRoughness = 3f;

        protected float minPersistance = 0.1f;
        protected float maxPersistance = 0.74f;

        protected float minMinValue = 0.3f;
        protected float maxMinValue = 3f;

        #endregion

        public SimpleNoiseSettings()
        {
            setNewMinMaxValues();
        }
        public void setNewPlanetType(Enums.PlanetType type)
        {
            planetType = type;
            setNewMinMaxValues();
        }
        public virtual void setNewMinMaxValues()
        {       
            switch(planetType)
            {
                case Enums.PlanetType.Flat: 
                    {
                        minStrenght = 0.01f;
                        maxStrenght = 0.5f;
                        minBaseRough = 0.1f;
                        maxBaseRough = 2.1f;
                        minRoughness = 0f;
                        maxRoughness = 3f;
                        minPersistance = 0.1f;
                        maxPersistance = 0.5f;
                        minMinValue = 1.8f;
                        maxMinValue = 2.5f;
                    } break;
                case Enums.PlanetType.Continential: 
                    {
                        minStrenght = 0.1f;
                        maxStrenght = 0.15f;
                        minBaseRough = 0.75f;
                        maxBaseRough = 1.25f;
                        minRoughness = 0.8f;
                        maxRoughness = 2.2f;
                        minPersistance = 0.44f;
                        maxPersistance = 0.5f;
                        minMinValue = 1f;
                        maxMinValue = 1.15f;
                    } break;
                case Enums.PlanetType.Islands:
                    {
                        minStrenght = 0.1f;
                        maxStrenght = 0.15f;
                        minBaseRough = 0.5f;
                        maxBaseRough = 1.65f;
                        minRoughness = 1f;
                        maxRoughness = 3f;
                        minPersistance = 0.35f;
                        maxPersistance = 0.42f;
                        minMinValue = 0.9f;
                        maxMinValue = 1.1f;
                    } break;
                default:
                case Enums.PlanetType.Random:
                    {
                        minStrenght = 0;
                        maxStrenght = 0.5f;
                        minBaseRough = 0;
                        maxBaseRough = 3f;
                        minRoughness = 0;
                        maxRoughness = 3f;
                        minPersistance = 0.1f;
                        maxPersistance = 0.5f;
                        minMinValue = persistance * 2.5f;
                        maxMinValue = persistance * 3f;
                    } break;
            }
        }

        public void RandomizeAllValues()
        {
            strenght = Random.Range(minStrenght, maxStrenght);
            baseRoughness = Random.Range(minBaseRough, maxBaseRough);
            roughness = Random.Range(minRoughness, maxRoughness);
            persistance = Random.Range(minPersistance, maxPersistance);
            RandomizeNoiseOrigin();
            minValue = Random.Range(minMinValue, maxMinValue);          
        }

        public void RandomizeAllValues(Enums.PlanetType type)
        {
            setNewPlanetType(type);
            strenght = Random.Range(minStrenght, maxStrenght);
            baseRoughness = Random.Range(minBaseRough, maxBaseRough);
            roughness = Random.Range(minRoughness, maxRoughness);
            persistance = Random.Range(minPersistance, maxPersistance);
            RandomizeNoiseOrigin();
            minValue = Random.Range(minMinValue, maxMinValue);
        }

        public void RandomizeNoiseOrigin()
        {
            center = new Vector3(Random.Range(0, 100), Random.Range(0, 100), Random.Range(0, 100));
        }
    }

    [System.Serializable]
    public class RigidNoiseSettings : SimpleNoiseSettings
    {
        public float weightMultiplier = 0.8f;

        public RigidNoiseSettings()
        {
            this.planetType = Enums.PlanetType.Random;
            setNewMinMaxValues();
        }

        public override void setNewMinMaxValues()
        {
            switch (planetType)
            {
                case Enums.PlanetType.Flat:
                    {
                        minStrenght = 0;
                        maxStrenght = 0.5f;
                        minBaseRough = 0;
                        maxBaseRough = 3f;
                        minRoughness = 0;
                        maxRoughness = 3f;
                        minPersistance = 0.1f;
                        maxPersistance = 0.5f;
                        minMinValue = persistance * 2.5f;
                        maxMinValue = persistance * 3f;
                    }
                    break;
                case Enums.PlanetType.Continential:
                    {
                        minStrenght = 0.17f;
                        maxStrenght = 0.35f;
                        minBaseRough = 0.4f;
                        maxBaseRough = 2.5f;
                        minRoughness = 0.4f;
                        maxRoughness = 2.5f;
                        minPersistance = 0.1f;
                        maxPersistance = 0.6f;
                        minMinValue = 0.25f;
                        maxMinValue = 1.25f;
                    }
                    break;
                case Enums.PlanetType.Islands:
                    {
                        minStrenght = 0.17f;
                        maxStrenght = 0.35f;
                        minBaseRough = 0.4f;
                        maxBaseRough = 2.5f;
                        minRoughness = 0.4f;
                        maxRoughness = 2.5f;
                        minPersistance = 0.1f;
                        maxPersistance = 0.6f;
                        minMinValue = 0.25f;
                        maxMinValue = 1.25f;
                    }
                    break;
                default:
                case Enums.PlanetType.Random:
                    {
                        minStrenght = 0;
                        maxStrenght = 0.5f;
                        minBaseRough = 0;
                        maxBaseRough = 3f;
                        minRoughness = 0;
                        maxRoughness = 3f;
                        minPersistance = 0.1f;
                        maxPersistance = 0.5f;
                        minMinValue = persistance * 2.5f;
                        maxMinValue = persistance * 3f;
                    }
                    break;
            }
        }

        public new void RandomizeAllValues()
        {
            strenght = Random.Range(minStrenght, maxStrenght);
            baseRoughness = Random.Range(minBaseRough, maxBaseRough);
            roughness = Random.Range(minRoughness, maxRoughness);
            persistance = Random.Range(minPersistance, maxPersistance);
            RandomizeNoiseOrigin();
            minValue = Random.Range(minMinValue, maxMinValue);
        }

        public new void RandomizeAllValues(Enums.PlanetType type)
        {
            setNewPlanetType(type);
            strenght = Random.Range(minStrenght, maxStrenght);
            baseRoughness = Random.Range(minBaseRough, maxBaseRough);
            roughness = Random.Range(minRoughness, maxRoughness);
            persistance = Random.Range(minPersistance, maxPersistance);
            RandomizeNoiseOrigin();
            minValue = Random.Range(minMinValue, maxMinValue);
        }
    }


    public NoiseSettings()
    {
        if(filterType == Enums.FilterType.Simple)
        {
            simpleNoiseSettings = new SimpleNoiseSettings();
        }
        else if (filterType == Enums.FilterType.Rigid)
        {
            rigidNoiseSettings = new RigidNoiseSettings();
        }
    }

    public NoiseSettings(Enums.FilterType fType)
    {
        filterType = fType;
        if (filterType == Enums.FilterType.Simple)
        {
            simpleNoiseSettings = new SimpleNoiseSettings();
        }
        else if (filterType == Enums.FilterType.Rigid)
        {
            rigidNoiseSettings = new RigidNoiseSettings();
        }
    }

}
