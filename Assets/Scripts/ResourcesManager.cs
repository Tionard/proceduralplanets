﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.U2D;

public static class ResourcesManager
{
    public static Texture2D[] texturesLand;
    public static List<Texture2D> texturesLandList = new List<Texture2D>();
    public static List<string> textureLandNames = new List<string>();

    public static Texture2D[] texturesWater;
    public static List<Texture2D> texturesWaterList = new List<Texture2D>();
    public static List<string> textureWaterNames = new List<string>();

    public static Material planetMaterial;
    public static Dictionary<string, ColourSettings> colorPresets = new Dictionary<string, ColourSettings>();
    public static Dictionary<string, ShapeSettings> shapePresets = new Dictionary<string, ShapeSettings>();
    public static List<string> presetNames = new List<string>();
    private static List<string> customPresetNames = new List<string>();

    public static void Initialize()
    {
        //Init planets Material
        planetMaterial = Resources.Load<Material>("PlanetMaterial");

        //Load base presets
        LoadBaseColorPresets();
        LoadBaseShapePresets();

#if !UNITY_EDITOR
        //Load user presets
        LoadCustomPresetList();
        LoadCustomColorPresets();
        LoadCustomShapePresets();
#endif

    }

    public static void UpdateTextures()
    {
        texturesLand = Resources.LoadAll<Texture2D>("Textures/Land");
        texturesLandList.AddRange(texturesLand);
        texturesWater = Resources.LoadAll<Texture2D>("Textures/Water");
        texturesWaterList.AddRange(texturesWater);

        foreach(Texture2D txL in texturesLandList)
        {
            textureLandNames.Add(txL.name);
        }
        foreach (Texture2D txW in texturesWaterList)
        {
            textureWaterNames.Add(txW.name);
        }
    }

    public static ColourSettings GetColorSetting(string key)
    {
        colorPresets[key].planetMaterial = planetMaterial;
        colorPresets[key].ResetLandNormal();
        colorPresets[key].ResetWaterNormal();
        colorPresets[key].planetMaterial.SetFloat("_smth", colorPresets[key].smoothness);
        return colorPresets[key];
    }

    public static ShapeSettings GetShapeSetting(string key)
    {
        return shapePresets[key];
    }

    public static void AddColorPreset(string name, ColourSettings settings)
    {
        if (colorPresets.ContainsKey(name))
        {
            colorPresets.Remove(name);
        }
        colorPresets.Add(name, settings);
    }

    public static void AddShapePreset(string name, ShapeSettings settings)
    {
        if (shapePresets.ContainsKey(name))
        {
            shapePresets.Remove(name);
        }
        shapePresets.Add(name, settings);
    }

    public static void LoadBaseColorPresets()
    {
        Object[] presets;
        presets = Resources.LoadAll<Object>("Presets/ColorPresets");

        foreach(Object preset in presets)
        {
            string name = preset.name.Replace("Color", "");
            if (!colorPresets.ContainsKey(name))
            {
                AddPresetName(name);
                ColourSettings settings = ScriptableObject.CreateInstance("ColourSettings") as ColourSettings;
                JsonUtility.FromJsonOverwrite(preset.ToString(), settings);
                colorPresets.Add(name, settings);
            }
        }
    }

    public static void LoadBaseShapePresets()
    {
        Object[] presets;
        presets = Resources.LoadAll<Object>("Presets/ShapePresets");

        foreach (Object preset in presets)
        {
            string name = preset.name.Replace("Shape", "");
            if (!shapePresets.ContainsKey(name))
            {
                AddPresetName(name);
                ShapeSettings settings = ScriptableObject.CreateInstance("ShapeSettings") as ShapeSettings;
                JsonUtility.FromJsonOverwrite(preset.ToString(), settings);
                shapePresets.Add(name, settings);
            }
            
        }
    }

    private static void LoadCustomPresetList()
    {
        string path;
#if UNITY_ANDROID
        path = Application.persistentDataPath + "/Resources/Presets/";
#else
        path = Application.dataPath + "/Resources/Presets/";
#endif
        ProvidePath(path);
        path += "/PresetList.json";
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "[]");
        }
        else
        {
            var rawJson = File.ReadAllText(path);
            customPresetNames = JsonConvert.DeserializeObject<List<string>>(rawJson);
        }
    }

    public static void LoadCustomShapePresets()
    {
        foreach (string customPresetName in customPresetNames)
        {
            if (!shapePresets.ContainsKey(customPresetName))
            {
                AddPresetName(customPresetName);
                string path;
#if UNITY_ANDROID
                path = Application.persistentDataPath + "/Resources/Presets/ShapePresets/" + customPresetName + "Shape.json";
#else
                path = Application.dataPath + "/Resources/Presets/ShapePresets/" + customPresetName + "Shape.json";
#endif
                if (File.Exists(path))
                {
                    var rawJsonShape = File.ReadAllText(path);
                    ShapeSettings settings = ScriptableObject.CreateInstance("ShapeSettings") as ShapeSettings;
                    JsonUtility.FromJsonOverwrite(rawJsonShape, settings);
                    shapePresets.Add(customPresetName, settings);
                }
            }
        }
    }

    public static void LoadCustomColorPresets()
    {
        foreach (string customPresetName in customPresetNames)
        {
            if (!colorPresets.ContainsKey(customPresetName))
            {
                AddPresetName(customPresetName);
                string path;
#if UNITY_ANDROID
                path = Application.persistentDataPath + "/Resources/Presets/ColorPresets/" + customPresetName + "Color.json";
#else
                path = Application.dataPath + "/Resources/Presets/ColorPresets/" + customPresetName + "Color.json";
#endif
                if (File.Exists(path))
                {
                    var rawJsonShape = File.ReadAllText(path);
                    ColourSettings settings = ScriptableObject.CreateInstance("ColourSettings") as ColourSettings;
                    JsonUtility.FromJsonOverwrite(rawJsonShape, settings);
                    colorPresets.Add(customPresetName, settings);
                }
            }
        }
    }

    public static void SaveCustomColorPreset(string name, ColourSettings preset)
    {

        AddColorPreset(name, preset);
        AddPresetName(name);

        if (!customPresetNames.Contains(name))
        {
            customPresetNames.Add(name);
        }
        string json = JsonUtility.ToJson(preset, true);
        string path;
#if UNITY_ANDROID
        path = Application.persistentDataPath + "/Resources/Presets/ColorPresets";
#else
        path = Application.dataPath + "/Resources/Presets/ColorPresets";
#endif
        ProvidePath(path);
        File.WriteAllText(path + "/" + name + "Color.json", json);

#if !UNITY_EDITOR
        SaveCustomPresetList();
#endif
    }

    public static void SaveCustomShapePreset(string name, ShapeSettings preset)
    {

        AddShapePreset(name, preset);
        AddPresetName(name);

        if (!customPresetNames.Contains(name))
        {
            customPresetNames.Add(name);
        }
        string json = JsonUtility.ToJson(preset, true);
        string path;
#if UNITY_ANDROID
        path = Application.persistentDataPath + "/Resources/Presets/ShapePresets";
#else
        path = Application.dataPath + "/Resources/Presets/ShapePresets";
#endif
        ProvidePath(path);
        File.WriteAllText(path + "/" + name + "Shape.json", json);

#if !UNITY_EDITOR
        SaveCustomPresetList();
#endif
    }

    private static void SaveCustomPresetList()
    {
        string json = JsonConvert.SerializeObject(customPresetNames, Formatting.Indented);
        string path;
#if UNITY_ANDROID
        path = Application.persistentDataPath + "/Resources/Presets";
#else
        path = Application.dataPath + "/Resources/Presets";
#endif
        ProvidePath(path);
        path += "/PresetList.json";
        File.WriteAllText(path, json);
    }

    private static void AddPresetName(string name)
    {
        if (!presetNames.Contains(name))
        {
            presetNames.Add(name);
        }
    }


    public static Texture2D getRandomLandTexture()
    {
        UpdateTextures();
        return texturesLand[Random.Range(0, texturesLand.Length - 1)];
    }

    public static Texture2D getRandomWaterTexture()
    {
        UpdateTextures();
        int index = Random.Range(0, texturesWater.Length);
        return texturesWater[index];
    }


    public static void ProvidePath(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
    }

    public static void ExportTexture(Material material)
    {
        Texture2D texture = (Texture2D) material.GetTexture("_texture");
        
        byte[] bytes = texture.EncodeToPNG();
        string path = Application.dataPath + "/Export/SaveImages/";
        ProvidePath(path);
        File.WriteAllBytes(path + "Image" + ".png", bytes);
    }
}
