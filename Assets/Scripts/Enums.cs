﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Enums
{
    public enum FilterType { Simple, Rigid };
    public enum SmoothnessType { Smooth, Rigid, Random };
    public enum PlanetType { Continential, Islands, Flat, Random }

}
