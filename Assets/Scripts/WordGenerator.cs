﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WordGenerator
{

    Dictionary<string, string> vowelsD = new Dictionary<string, string>();
    Dictionary<string, string> consonantsD = new Dictionary<string, string>();
    private string[] vowels = { "a", "e", "o", "i", "u", "y" };
    private string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z" };
    public string[] letters;

    public string firstLetter;
    public string secondLetter;
    public string secondLastLetter;
    public string lastLetter;

    public string word;
    private string firstPart;
    private string middlePart;
    private string lastPart;

    private int VowInRow = 0;
    private int ConInRow = 0;

    public WordGenerator()
    {
        Initialize();
        UpdateLetters();
    }

    void Initialize()
    {
        foreach(string vowel in vowels)
        {
            vowelsD.Add(vowel, vowel);
        }
        foreach (string consonant in consonants)
        {
            consonantsD.Add(consonant, consonant);
        }
    }


    public string CreateRandomWord(int wordLenght, int maxVowInRow, int maxConInRow)
    {
        string currentSymbol = "";
        this.middlePart = "";
        this.word = "";

        int startPositionShift = 0;
        int lastPositionShift = 0;
        /*
        if (firstLetter != null)
            firstLetter = firstLetter.ToUpper();
        AddOptionalSymbol(firstLetter, ref firstPart, ref startPositionShift, ref VowInRow, ref ConInRow);
        AddOptionalSymbol(secondLetter, ref firstPart, ref startPositionShift, ref VowInRow, ref ConInRow);
        AddOptionalSymbol(secondLastLetter, ref lastPart, ref lastPositionShift);
        AddOptionalSymbol(lastLetter, ref lastPart, ref lastPositionShift);

        */

        word += firstPart;
        
        for (int i = 0 + startPositionShift; i < (wordLenght - lastPositionShift); i++)
        {
            var randomVowelNumber = Random.Range(0, vowelsD.Count);
            var randomConsonantNumber = Random.Range(0, consonantsD.Count);

            if (VowInRow < maxVowInRow && ConInRow < maxConInRow)
            {

                bool vowelLetter = Random.value < 0.5;
                if (vowelLetter)
                {

                    currentSymbol = (string)vowelsD.Values.ElementAt(randomVowelNumber);
                    VowInRow++;
                }
                else
                {
                    currentSymbol = (string)consonantsD.Values.ElementAt(randomConsonantNumber);
                    ConInRow++;
                }
            }
            else if (VowInRow < maxVowInRow)
            {
                currentSymbol = (string)vowelsD.Values.ElementAt(randomVowelNumber);
                VowInRow++;
                ConInRow = 0;
            }
            else if (ConInRow < maxConInRow)
            {
                currentSymbol = (string)consonantsD.Values.ElementAt(randomConsonantNumber);
                ConInRow++;
                VowInRow = 0;
            }

            /*
            if (i == 0)
                currentSymbol = currentSymbol.ToUpper();
            */

            middlePart += currentSymbol;

        }
        this.word += middlePart + lastPart;

        CapitalizeFirstLetter();

        VowInRow = 0;
        ConInRow = 0;
        return this.word;
    }

    bool AddOptionalSymbol(string symbol, ref string textPart, ref int positionShift, ref int mainTypeCount, ref int secondaryTypeCount)
    {
        if (symbol != null && symbol.ToUpper() != "ANY")
        {
            positionShift++;
            textPart += symbol;
            if (IsVowelSymbol(symbol))
            {
                mainTypeCount++;
                secondaryTypeCount = 0;
            }
            else
            {
                mainTypeCount = 0;
                secondaryTypeCount++;
            }

            return true;
        }
        else
            return false;
    }

    void CapitalizeFirstLetter()
    {
        this.word = this.word.First().ToString().ToUpper() + this.word.Substring(1).ToLower();
    }

    string CapitalizeFirstLetter(string custom)
    {
        custom = custom.First().ToString().ToUpper() + custom.Substring(1).ToLower();
        return custom;
    }

    bool AddOptionalSymbol(string symbol, ref string textPart, ref int positionShift)
    {
        if (symbol != null && symbol.ToUpper() != "ANY")
        {
            positionShift++;
            textPart += symbol;

            return true;
        }
        else
            return false;
    }

    public void SetFirstPart(string optionalPart)
    {

        this.firstPart = optionalPart;
        foreach(char ch in optionalPart)
        {
            if (IsInVowelValues(ch.ToString().ToLower()))
            {
                VowInRow++;
                ConInRow = 0;
            }
            else
            {
                ConInRow++;
                VowInRow = 0;
            }
        }

    }

    public void SetLastPart(string optionalPart)
    {
        this.lastPart = optionalPart;
    }

    void UpdateLetters()
    {
        letters = new string[vowelsD.Count + consonantsD.Count];
        int i = 0;
        foreach (string symbol in vowelsD.Values)
        {
            letters[i] = symbol;
            i++;
        }
        foreach(string symbol in consonantsD.Values)
        {
            letters[i] = symbol;
            i++;
        }
    }

    public void ToggleSymbol(string symbol)
    {
        if(symbol != null)
        {
            if (IsInVowelValues(symbol))
            {
                vowelsD.Remove(symbol);
                Debug.Log("Symbol " + symbol + " was removed from Vovels Dictionary");
            }
            else if(IsInConsonantValues(symbol))
            {
                consonantsD.Remove(symbol);
                Debug.Log("Symbol " + symbol + " was removed from Consonants Dictionary");
            }
            else if(IsVowelSymbol(symbol))
            {
                vowelsD.Add(symbol, symbol);
                Debug.Log("Symbol " + symbol + " was added to Vovels Dictionary");
            }
            else if (IsConsonantSymbol(symbol))
            {
                consonantsD.Add(symbol, symbol);
                Debug.Log("Symbol " + symbol + " was added to Consonants Dictionary");
            }
        }
        UpdateLetters();
    }

    public string ShuffleMiddlePart()
    {
        this.middlePart = new string(this.middlePart.ToCharArray().OrderBy(x => System.Guid.NewGuid()).ToArray());
        
        return CapitalizeFirstLetter(this.firstPart + this.middlePart + this.lastPart);
    }

    public string ShuffleWord()
    {
        this.word = new string(this.word.ToCharArray().OrderBy(x => System.Guid.NewGuid()).ToArray());
        return CapitalizeFirstLetter(this.word);
    }

    public string ShuffleCustomWord(ref string customWord)
    {
        customWord = new string(customWord.ToCharArray().OrderBy(x => System.Guid.NewGuid()).ToArray());
        return CapitalizeFirstLetter(customWord);
    }

    public bool IsVowelSymbol(string symbol)
    {
        foreach (string str in vowels)
        {
            if (symbol.ToLower() == str)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsConsonantSymbol(string symbol)
    {
        foreach (string str in consonants)
        {
            if (symbol.ToLower() == str)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsInVowelValues(string symbol)
    {
        foreach (string str in vowelsD.Values)
        {
            if (symbol.ToLower() == str)
            {
                return true;
            }
        }
        return false;
    }

    public bool IsInConsonantValues(string symbol)
    {
        foreach (string str in consonantsD.Values)
        {
            if (symbol.ToLower() == str)
            {
                return true;
            }
        }
        return false;
    }


}
