﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NoiseFilterFactory 
{
    public static INoiseFilter CreateNoiseFilter(NoiseSettings settings)
    {
        switch (settings.filterType)
        {
            case Enums.FilterType.Simple:
                return new SimpleNoiseFilter(settings.simpleNoiseSettings);
            case Enums.FilterType.Rigid: 
                return new RidgidNoiseFilter(settings.rigidNoiseSettings);
        }
        return null;
    }
}
