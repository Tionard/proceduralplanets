﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchActive : MonoBehaviour
{
    public GameObject target1;
    public GameObject target2;

    public GameObject conditionalObject;

    public void SwitchActiveElement()
    {
        target1.SetActive(!target1.activeSelf);
        target2.SetActive(!target2.activeSelf);
    }

    public void SwitchActiveElementConditionaly()
    {
        if(conditionalObject != null)
        {
            target1.SetActive(conditionalObject.activeSelf);
            target2.SetActive(!conditionalObject.activeSelf);
        }
        else
        {
            Debug.LogError("Add Conditional Object first!");
        }

    }
}
