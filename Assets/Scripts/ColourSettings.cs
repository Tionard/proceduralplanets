﻿using NaughtyAttributes;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Planet/PlanetColor")]
public class ColourSettings : ScriptableObject
{
    //[System.NonSerialized]
    public Material planetMaterial;

    [HorizontalLine(color: EColor.Red)]
    public BiomeColourSettings biomeColourSettings;
    public Gradient oceanColour;
    public Texture2D landNormalMap;
    public string landNormalMapName;
    public float landNMTiling;
    public Texture2D waterNormalMap;
    public string waterNormalMapName;
    public float waterNMTiling;

    public float smoothness = 0;

    [System.Serializable]
    public class BiomeColourSettings
    {
        public Biome[] biomes = new Biome[1];
        public NoiseSettings noise;
        public float noiseOffset = 0;
        public float noiseStrength = 0;
        [Range(0,1)]
        public float blendAmount = 0;

        [System.Serializable]
        public class Biome
        {
            public Gradient gradient = new Gradient();
            public Color tint = new Color();
            [Range(0, 1)]
            public float startHeight = 0;
            [Range(0, 1)]
            public float tintPrecent = 0;
        }

        public BiomeColourSettings()
        {
            noise = new NoiseSettings(Enums.FilterType.Simple);
            for(int i = 0; i < biomes.Length; i++)
            {
                biomes[i] = new Biome();
            }
        }
    }

    public void Initialize()
    {
        planetMaterial = ResourcesManager.planetMaterial;
        biomeColourSettings = new BiomeColourSettings();
        oceanColour = CreateRandomGradient(Enums.SmoothnessType.Smooth);
        waterNormalMap = ResourcesManager.getRandomWaterTexture();
        waterNMTiling = Random.Range(10, 25);
        planetMaterial.SetTexture("_normalWater", waterNormalMap);
        planetMaterial.SetFloat("_tilingLand", waterNMTiling);
        landNormalMap = ResourcesManager.getRandomLandTexture();
        landNMTiling = Random.Range(4, 25);
        planetMaterial.SetTexture("_normalLand", landNormalMap);
        planetMaterial.SetFloat("_tilingLand", landNMTiling);
    }

    public void AssignRandomLandNormalMap()
    {
        Texture2D[] textures = Resources.LoadAll<Texture2D>("Textures");
        landNormalMap = textures[Random.Range(0, textures.Length-1)];
    }

    [HorizontalLine(color: EColor.Red)]
    public bool generationMode;

    [ShowIf("generationMode")]
    public Enums.SmoothnessType gradientSmoothnessType = Enums.SmoothnessType.Smooth;

    public void CreateRandomBiomes(Enums.SmoothnessType sType)
    {
        float startHeight = 0;
        int numberOfBiomes = Random.Range(1, 5);
        float sectionSize = (1.0f/(float)numberOfBiomes);   

        biomeColourSettings.biomes = new BiomeColourSettings.Biome[numberOfBiomes];
        RandomizeLandNormal();
        if (numberOfBiomes > 1)
        {
            biomeColourSettings.blendAmount = Random.Range(0.1f, 0.75f);
        }

        for(int i = 0; i < numberOfBiomes; i++)
        {            
            bool addTint = Random.value > 0.75;
            
            biomeColourSettings.biomes[i] = new BiomeColourSettings.Biome();
            biomeColourSettings.biomes[i].gradient = CreateRandomGradient(sType);
            biomeColourSettings.biomes[i].startHeight = startHeight;

            if (addTint)
            {
                biomeColourSettings.biomes[i].tintPrecent = Random.Range(0.0f, 0.5f);
                biomeColourSettings.biomes[i].tint = CreateRandomColor();
            }

            startHeight += sectionSize;
        }
        
    }

    public void RandomizeLandColor(Enums.SmoothnessType sType)
    {
        foreach(BiomeColourSettings.Biome b in biomeColourSettings.biomes)
        {
            b.gradient = CreateRandomGradient(sType);
        }
    }

    public void RandomizeOceanColor(Enums.SmoothnessType sType)
    {
        oceanColour = CreateRandomGradient(sType);
        smoothness = Random.Range(0.0f, 1.0f);
        SetReflectiveness();
        RandomizeWaterNormal();
    }

    protected Gradient CreateRandomGradient(Enums.SmoothnessType type)
    {
        Gradient gradient = new Gradient();
        int numberOfKeys = Random.Range(2, 8);
        GradientColorKey[] colorKey = new GradientColorKey[numberOfKeys];

        float timeStamp = 0.0f;
        float[] randomTimeArray = RandomizeFloatArray(numberOfKeys);
        float sectionSize = (1.0f / (float)(numberOfKeys - 1));
        for (int i = 0; i < numberOfKeys; i++)
        {    
            if(type == Enums.SmoothnessType.Smooth)
            {
                colorKey[i].time = timeStamp;
                timeStamp += sectionSize;
            }
            else if (type == Enums.SmoothnessType.Rigid)
            {
                if (i == 0)
                {
                    colorKey[i].time = 0.0f;
                }
                else if (i == numberOfKeys - 1)
                {
                    colorKey[i].time = 1.0f;
                }
                else
                {
                    colorKey[i].time = timeStamp + Random.Range(0.01f, (1.0f - timeStamp) / 2.0f);
                    timeStamp += sectionSize;
                }
            }
            else if (type == Enums.SmoothnessType.Random)
            {
                timeStamp = randomTimeArray[i];
                colorKey[i].time = timeStamp;                
            }

            colorKey[i].color = CreateRandomColor();      
        }
        if(oceanColour != null)
        {
            gradient.SetKeys(colorKey, oceanColour.alphaKeys);
        }
        else
        {
            GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
            alphaKeys[0].alpha = alphaKeys[1].alpha = 1f;
            alphaKeys[0].time = 0f;
            alphaKeys[1].time = 1f;
            gradient.SetKeys(colorKey, alphaKeys);
        }
            
        gradient.mode = GradientMode.Blend;
        return gradient;
    }

    private float[] RandomizeFloatArray(int arraySize)
    {
        float[] array = new float[arraySize];
        List<float> arrayList = new List<float>();
        for(int i = 0; i < arraySize; i++)
        {
            arrayList.Add((float)Random.Range(0.01f, 0.99f));     
        }
        arrayList.Sort();
        array = arrayList.ToArray();
        return array;
    }

    public Color CreateRandomColor()
    {
        Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        return color;
    }

    public void SetNewLandNormal(Texture2D texture, float tiling)
    {
        planetMaterial.SetTexture("_normalLand", texture);
        planetMaterial.SetFloat("_tilingLand", tiling);
        landNormalMap = texture;
        landNMTiling = tiling;
        landNormalMapName = texture.name;
    }

    public void SetNewWaterNormal(Texture2D texture, float tiling)
    {
        planetMaterial.SetTexture("_normalWater", texture);
        planetMaterial.SetFloat("_tilingWater", tiling);
        waterNormalMap = texture;
        waterNMTiling = tiling;
        waterNormalMapName = texture.name;
    }

    public void ResetLandNormal()
    {
        Texture2D texture = ResourcesManager.texturesLandList.Find(txL => txL.name == landNormalMapName);
        SetNewLandNormal(texture, landNMTiling);
    }

    public void ResetWaterNormal()
    {
        Texture2D texture = ResourcesManager.texturesWaterList.Find(txW => txW.name == waterNormalMapName);
        SetNewWaterNormal(texture, waterNMTiling);
    }

    public void RandomizeLandNormal()
    {
        Texture2D texture = ResourcesManager.getRandomLandTexture();
        float tiling = Random.Range(4, 20);
        SetNewLandNormal(texture, tiling);
    }

    public void RandomizeWaterNormal()
    {
        Texture2D texture = ResourcesManager.getRandomWaterTexture();
        float tiling = Random.Range(4, 20);
        SetNewWaterNormal(texture, tiling);
    }
    
    private void SetReflectiveness()
    {
        planetMaterial.SetFloat("_smth", smoothness);
    }

    public void SetReflectiveness(float smth)
    {
        smoothness = smth;
        SetReflectiveness();       
    }
}
