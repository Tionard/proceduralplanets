# Procedural Planets

You can play online or download it from my portfolio page: [tionard.itch.io](https://tionard.itch.io) 

Procedural Planets is a Unity-based prototype that lets you generate, modify, and save stylized planets and moons. The tool offers a large amount of control over the generation process, allowing you to create unique, procedurally generated celestial bodies with customizable biomes and terrain features.

## Features

- **Planet & Moon Generation:**  
  Create planets or moons with random biomes and varied landmass patterns using different shape types (e.g., Continental, Islands, Flat, or Random).

- **Terraforming Tools:**  
  Modify the generated planets by adjusting noise parameters such as noise strength, roughness, persistence, and base noise level for fine-tuned terrain control.

- **Material & Biome Customization:**  
  Design unique biomes by editing color gradients and normal maps for both land and ocean. Adjust tiling and reflectiveness to get the desired visual style.

- **Preset Management:**  
  Save and load planet presets—including mesh and material settings—to easily recreate or share your custom creations.  
  **Note:** Custom presets are preserved only in the standalone version. The integrated web version does not retain saved data between sessions.

- **Advanced Settings:**  
  Explore advanced mesh and material settings for deeper control, such as adjusting Level of Detail (LOD) and noise layers, ensuring that even subtle changes can produce dramatic differences.

## Usage

Once you launch Procedural Planets on your preferred platform (HTML5, Windows, or Android), you can explore its features through several editor tabs:

- **Generation:**  
  Select a shape type, generate random shapes and biomes, and randomize origins or gradients to create the base structure of your planet.

- **Advanced Mesh Settings:**  
  Control the quality and detail of the planet’s mesh by adjusting LOD and noise parameters. (Tip: Use a lower LOD when experimenting with "Advanced Mesh Settings" to speed up mesh reconstruction.)

- **Advanced Material Settings:**  
  Customize land and ocean appearances by choosing biomes, editing gradients, and applying normal maps.

- **General Settings:**  
  Adjust camera distance, planet rotation (including speed and direction), and background music volume to tailor your viewing experience.

- **Save/Load Preset:**  
  Save your customized planet settings or load previously saved configurations to quickly switch between designs.

## Credits

- **Developer:** Igor Kocharovski (me), visit my portfolio: [tionard.itch.io](https://tionard.itch.io) 
- **Inspiration:** Massive inspiration was drawn from Sebastian Lague's procedural generation videos.

## License

- **Code:** MIT License  
- **Assets:** Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International

## Feedback

Feel free to leave your feedback or suggestions on the [project page](https://tionard.itch.io/procedural-planets). Your comments and constructive critiques are greatly appreciated!
